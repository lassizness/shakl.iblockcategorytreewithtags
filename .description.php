<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = [
    'NAME' => GetMessage("SHAKL_COMPONENT_NAME"),
    'DESCRIPTION' => GetMessage("SHAKL_COMPONENT_DESCRIPTION"),
    'PATH' => [
        'ID' => 'shakl',
        'NAME' => 'SHAKL',
    ],
];