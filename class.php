<?php
use Bitrix\Main\Loader;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Data\Cache;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CategoryTreeWithTags extends CBitrixComponent
{
    /**
     * Получает разделы и элементы инфоблока, фильтруя их по активности и принадлежности к указанному инфоблоку.
     * Для каждого элемента извлекаются его название и теги. Теги предполагается хранить в виде строки, разделённой запятыми.
     * Результаты (разделы с элементами) сохраняются в $this->arResult['SECTIONS'].
     * Пустые разделы (без элементов) из результата исключаются.
     *
     * @return void Метод не возвращает значение, результат работы сохраняется в $this->arResult['SECTIONS'].
     */
    protected function getSectionsWithElements()
    {
        Loader::includeModule('iblock');

        $sections = [];
        $elements = [];

        $sectionResult = SectionTable::getList([
            'select' => ['ID', 'NAME'],
            'filter' => ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'],
            'order' => ['SORT' => 'ASC']
        ]);
        while ($section = $sectionResult->fetch()) {
            $sections[$section['ID']] = [
                'NAME' => $section['NAME'],
                'ITEMS' => []
            ];
        }

        $elementResult = ElementTable::getList([
            'select' => ['ID', 'NAME', 'IBLOCK_SECTION_ID', 'TAGS'],
            'filter' => ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'],
            'order' => ['SORT' => 'ASC']
        ]);
        while ($element = $elementResult->fetch()) {
            if (isset($sections[$element['IBLOCK_SECTION_ID']])) {
                $elements[$element['ID']] = [
                    'NAME' => $element['NAME'],
                    'TAGS' => explode(',', $element['TAGS']) // Предполагаем, что теги хранятся в виде строки, разделенной запятыми
                ];
                $sections[$element['IBLOCK_SECTION_ID']]['ITEMS'][$element['ID']] = $elements[$element['ID']];
            }
        }

        $this->arResult['SECTIONS'] = array_filter($sections, function ($section) {
            return count($section['ITEMS']) > 0;
        });
    }

    /**
     * Генерирует уникальный идентификатор кеша для компонента на основе его параметров и идентификатора сайта.
     * Используется для определения уникальности кеша в контексте заданных параметров компонента и сайта, на котором он запущен.
     * Это позволяет избежать конфликтов кеша при использовании одного и того же компонента с разными параметрами или на разных сайтах.
     *
     * @return string Возвращает строку, представляющую собой уникальный идентификатор кеша для текущих параметров компонента и сайта.
     */
    public function getCacheId()
    {
        return serialize($this->arParams) . '/' . SITE_ID;
    }

    /**
     * Основной метод компонента, который выполняет логику построения дерева категорий и элементов из инфоблока.
     * Проверяет наличие модуля инфоблоков, инициализирует процесс кеширования, получает данные разделов и элементов,
     * а также регистрирует теги кеша для автоматического инвалидирования кеша при изменениях в инфоблоке.
     * В случае успешного получения данных из кеша, или после их получения и обработки, подключает шаблон компонента для отображения.
     *
     * @throws \Bitrix\Main\LoaderException Если модуль инфоблоков не найден, выбрасывает исключение.
     *
     * @return void Функция не возвращает значение, но результат работы сохраняется в $this->arResult и выводится через шаблон компонента.
     */
    public function executeComponent()
    {
        if (!Loader::includeModule('iblock')) {
            throw new \Bitrix\Main\LoaderException("Модуль Инфоблоков не найден.");
        }

        $cache = Cache::createInstance();

        if ($cache->initCache($this->arParams['CACHE_TIME'], $this->getCacheId(), '/category_tree_with_tags/')) {
            $this->arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $this->getSectionsWithElements();

            global $CACHE_MANAGER;

            $CACHE_MANAGER->StartTagCache('/category_tree_with_tags/');
            $CACHE_MANAGER->RegisterTag('iblock_id_' . $this->arParams['IBLOCK_ID']);
            $CACHE_MANAGER->EndTagCache();

            $cache->endDataCache($this->arResult);
        }

        $this->includeComponentTemplate();
    }
}