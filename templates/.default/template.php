<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['SECTIONS'] as $section) {
    echo "<b>-- {$section['NAME']}</b><br>";
    foreach ($section['ITEMS'] as $item) {
        echo "---- {$item['NAME']} (" . implode(', ', $item['TAGS']) . ")<br>";
    }
}
